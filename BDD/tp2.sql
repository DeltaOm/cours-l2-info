#1. References et noms des clients qui habitent Namur ou Toulouse.

/*
SELECT RefC, NomC
FROM CLIENT
WHERE UPPER(Ville) = "TOULOUSE"
OR UPPER(Ville) = "NAMUR"
;
*/

#2. Quantité totale de chevilles en stock (en considérant tous les produits de type 'cheville').

/*
SELECT TypeP, SUM(QStock)
FROM PRODUIT
WHERE TypeP = "Cheville"
GROUP BY TypeP
;
*/

#3. Pour chaque commande, le prix du produit le plus cher qu'elle comporte'.

/*
SELECT D.RefCom, MAX(P.Prix)
FROM DETAIL D, PRODUIT P
WHERE D.RefP = P.RefP
GROUP BY D.RefCom
;
*/

#4. Noms et références des clients qui ont passé plus de deux commandes.

/*
SELECT C.RefC, C.NomC
FROM CLIENT C, COMMANDE COM
WHERE C.RefC = COM.RefC
GROUP BY C.RefC, C.NomC
HAVING COUNT(COM.RefC) >= 2
;
*/


#5 Références des commandes qui comportent plus de deux produits de type "clou".

/*
SELECT D.RefCom
FROM DETAIL D, PRODUIT P
WHERE P.RefP = D.RefP
AND P.TypeP = "Clou"
GROUP BY D.RefCom
HAVING COUNT(P.TypeP) >=2
;
*/

#6. Villes dont le nombre de clients qui y habitent est supérieur au nombre de clients qui habitent Toulouse

/*
SELECT Ville
FROM CLIENT
GROUP BY Ville
HAVING COUNT(*) > (SELECT COUNT(*) FROM CLIENT WHERE Ville = "Toulouse")
;
*/

#7.Noms des clients qui ont passé des commandes ne comportant aucun produit à 220 euros

/*
SELECT DISTINCT C.NomC
FROM CLIENT C, COMMANDE COM
WHERE C.RefC = COM.RefC
AND COM.RefC NOT IN (SELECT COM.RefC
			FROM PRODUIT P, COMMANDE COM, DETAIL D
			WHERE COM.RefCom = D.RefCom
			AND D.RefP = P.RefP
			AND P.Prix = 220
		)
;
*/


#8.Noms des clients qui n"ont commandé ni clou, ni planche.
/*
SELECT DISTINCT C.NomC
FROM CLIENT C
WHERE C.NomC NOT IN (SELECT C.NomC
			FROM CLIENT C, COMMANDE COM, PRODUIT P, DETAIL D
			WHERE C.RefC = COM.RefC
			AND COM.RefCom = D.RefCom
			AND D.RefP = P.RefP
			AND (P.TypeP = "Clou" OR P.TypeP = "Planche")
			)
;
*/


#9. Villes où personne n"a commandé le produit "CL45".
/*
SELECT DISTINCT Ville
FROM CLIENT
WHERE Ville NOT IN (SELECT C.Ville 
			FROM CLIENT C, COMMANDE COM, DETAIL D
			WHERE C.RefC = COM.RefC
			AND COM.RefCom = D.RefCom
			AND D.RefP = "CL45"
			)
;
*/

#10. Paires de clients qui habitent dans la même ville( chaque paire ne doit être représentée qu'une seule fois')

/*
SELECT C1.NomC, C2.NomC
FROM CLIENT C1, CLIENT C2
WHERE C1.Ville = C2.Ville
AND C1.RefC < C2.RefC
;
*/

#11. Paires de produits qui n"ont jamais été commandés ensemble (chaque paire ne doit être représentée qu"une seule fois)

/*
SELECT DISTINCT P1.RefP, P2.RefP
FROM PRODUIT P1,PRODUIT P2
WHERE P1.RefP < P2.RefP
AND (P1.RefP, P2.RefP) NOT IN (SELECT P1.RefP,P2.RefP
				FROM PRODUIT P1, PRODUIT P2, DETAIL D1, DETAIL D2
				WHERE P1.RefP= D1.RefP AND P2.RefP = D2.RefP
				AND D1.RefCom = D2.RefCom)			
;
*/
