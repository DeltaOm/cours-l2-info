#Exercice 1
#Partie 2
#2 Quantité totale de chevilles commandées par des clients de Toulouse (c’est-`a-dire,
#la somme des quantités de chevilles commandées par l’ensemble des clients de Tou-
#5
#louse)`.
#Peut-on utiliser NATURAL JOIN ? Pourquoi ?

SELECT SUM(Quantite)
FROM CLIENT NATURAL JOIN COMMANDE NATURAL JOIN DETAIL NATURAL JOIN PRODUIT
WHERE Ville='Toulouse'
AND TypeP='Cheville'
;

#2 Code des produits ayant une réduction non nulle avec leur pourcentage de réduction
#classés par ordre croissant de réduction.

SELECT Code,Reduction
FROM REDUCTION
WHERE Reduction > 0
ORDER BY Reduction
;

#3 Pour chaque produit commandé, nombre de commandes où ce produit a été commandé avec une réduction non null

SELECT D.RefP, COUNT(D.RefCom)
FROM DETAIL D, REDUCTION R
WHERE D.RefP = R.Code
AND D.Quantite >= R.Quantite
AND R.Reduction > 0
GROUP BY D.RefP
;

#4 Que retourne la requete suivante ? Pourquoi ? 

SELECT *
FROM REDUCTION NATURAL JOIN DETAIL
;

#+----------+-------+-----------+--------+-------+
#| Quantite | Code  | Reduction | RefCom | RefP  |
#+----------+-------+-----------+--------+-------+
#|      120 | PL222 |       7.1 |      4 | CH464 |
#+----------+-------+-----------+--------+-------+
#NATURAL JOIN fait un = sur les noms de colonne semblable, ici Quantité, alors qu'on' aimerai
#plutot avoir sur Code/RefP

#Exercice 2
UPDATE CLIENT
SET CAT = 'C1'
WHERE NomC='Mercier'
;

UPDATE CLIENT
SET CAT = 'C2'
WHERE NomC='Neuman'
;

#Exercice 3
#1 Références et noms des clients qui ont chacun commandé plus de 50 clous, en considérant
#toutes les commandes de chaque clients
SELECT RefC, NomC, SUM(Quantite) as QuantiteClous
FROM CLIENT NATURAL JOIN COMMANDE NATURAL JOIN DETAIL NATURAL JOIN PRODUIT
WHERE TypeP='Clou'
GROUP BY RefC
HAVING QuantiteClous > 50
;

#2 Noms des clients qui n'ont' commandé aucun des produits commandés par Vanderka
#Y a pas moyen de le faire sans la double requête imbriquée ?
SELECT NomC
FROM CLIENT
WHERE NomC NOT IN (SELECT NomC
			FROM CLIENT NATURAL JOIN COMMANDE NATURAL JOIN DETAIL
			WHERE RefP NOT IN (SELECT RefP
						FROM CLIENT NATURAL JOIN COMMANDE NATURAL JOIN DETAIL
						WHERE NomC='Venderka'
			)
)
ORDER BY NomC
;

#3 Moyenne du montant total d une commande, en considérant toutes les commandes
SELECT AVG(Somme)
FROM (
	SELECT SUM(Prix*Quantite) as Somme
	FROM DETAIL NATURAL JOIN PRODUIT
	GROUP BY RefCom
) AS calcul_somme
;

#4 Références de la (ou des) commande(s) dont le montant total est maximal parmi
#toutes les commandes. On donnera aussi ce montant

SELECT RefCom, SUM(Prix*Quantite) as Total
FROM COMMANDE NATURAL JOIN DETAIL NATURAL JOIN PRODUIT
GROUP BY RefCom
HAVING Total = ALL (SELECT MAX(Somme)
			FROM (SELECT SUM(Prix*Quantite) as Somme
				FROM DETAIL NATURAL JOIN PRODUIT
				GROUP BY RefCom
				) as calcul_somme
			)
; 

#5 Noms des clients qui ont commandé le produit CL45 ou le produit CL60,
#mais pas les deux. Indication : Ces clients n'ont' commandé qu'un' seul produit.

SELECT NomC
FROM CLIENT NATURAL JOIN COMMANDE NATURAL JOIN DETAIL
WHERE RefP='CL45' OR RefP='CL60'
GROUP BY NomC
HAVING COUNT(RefP) = 1
;

#6 Noms des clients qui ont commandé exactement deux des trois produits CL45, CL60 et CH464

SELECT NomC
FROM CLIENT NATURAL JOIN COMMANDE NATURAL JOIN DETAIL
WHERE RefP='CL45' OR RefP='CL60' OR RefP = 'CH464'
GROUP BY NomC
HAVING COUNT(RefP) = 2
;

#7 Pour chaque client (représenté par sa référence), le nombre total de clous qu’il a
#commandés (en considérant toutes les commandes). Ne pas oublier les clients qui
#n’ont commandé aucun clou.

SELECT RefC, SUM(Quantite)
FROM CLIENT NATURAL JOIN COMMANDE NATURAL JOIN DETAIL NATURAL JOIN PRODUIT
WHERE TypeP = 'Clou'
GROUP BY RefC

UNION

SELECT RefC, 0
FROM CLIENT
WHERE RefC NOT IN (SELECT RefC
			FROM CLIENT NATURAL JOIN COMMANDE NATURAL JOIN DETAIL NATURAL JOIN PRODUIT
			WHERE TypeP = 'Clou'
			)
			GROUP BY RefC
;

--8 Pour chaque ville et chaque catégorie de clients de cette ville, 
-- la somme, en euros, des montants de toutes les commandes des clients 
-- habitant cette ville et appartenant à cette catégorie.
-- Ne pas oublier les couples (ville, catégorie) pour lesquels
-- aucun client n’a fait de commande et pour lesquels le montant total
-- sera donc 0.

SELECT Ville, CAT, SUM(Quantite*Prix)
FROM CLIENT NATURAL JOIN COMMANDE NATURAL JOIN DETAIL NATURAL JOIN PRODUIT
GROUP BY Ville,CAT

UNION

SELECT Ville, CAT, 0
FROM CLIENT
WHERE (Ville,CAT) NOT IN (SELECT Ville, CAT
				FROM CLIENT NATURAL JOIN COMMANDE NATURAL JOIN DETAIL NATURAL JOIN PRODUIT
				GROUP BY Ville,CAT
				)
;
				

