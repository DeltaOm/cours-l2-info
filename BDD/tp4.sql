#Exercice 2

#1 Références et noms des clients qui ont chacun commandé au total (c’est-à-dire,
#en considérant toutes les commandes) un nombre de chevilles plus grand que le
#nombre total de clous command ́es par l’ensemble des clients.

SELECT C.RefC,C.NomC, SUM(Quantite) as QuantiteCheville
FROM CLIENT C, COMMANDE Com, DETAIL D, PRODUIT P
WHERE C.RefC = Com.RefC AND Com.RefCom = D.RefCom AND D.RefP = P.RefP
AND P.TypeP = 'Cheville'
GROUP BY C.RefC
HAVING SUM(Quantite) > ( SELECT SUM(Quantite)
			FROM DETAIL D, PRODUIT P
			WHERE D.RefP = P.RefP
			AND P.TypeP = 'Clou'
			)
;

#2 Produits de type "Cheville" les plus commandés (en quantité totale) et cette quantité.

SELECT P.RefP,SUM(Quantite) as Quantite
FROM DETAIL D, PRODUIT P
WHERE P.RefP = D.RefP
AND TypeP = 'Cheville'
GROUP BY P.RefP
HAVING Quantite >= ALL (SELECT SUM(Quantite)
			FROM DETAIL NATURAL JOIN PRODUIT
			WHERE TypeP='Cheville'
			GROUP BY RefP)
;

-- 3. Pour chaque client ayant effectué au moins une commande,
-- nom du client et pourcentage de produits qu'il a acheté sur l'ensemble
-- de ses commandes par rapport au nombre total de produits.
-- On affichera le résultat par ordre décroissant du pourcentage.

SELECT NomC, (COUNT(DISTINCT RefP)*100/(SELECT COUNT(*) FROM PRODUIT)) AS Pourcentage
FROM CLIENT NATURAL JOIN COMMANDE NATURAL JOIN DETAIL
GROUP BY NomC
ORDER BY Pourcentage DESC
;

-- 4. Références des clients qui ont commandé au moins une fois chaque
-- type de produit. On donnera deux solutions (l'une avec COUNT,
-- l'autre avec un double NOT IN).

SELECT RefC
FROM COMMANDE NATURAL JOIN DETAIL NATURAL JOIN PRODUIT
GROUP BY RefC
HAVING COUNT(DISTINCT TypeP) = (SELECT COUNT(DISTINCT TypeP) FROM PRODUIT)
;


SELECT RefC
FROM COMMANDE NATURAL JOIN DETAIL NATURAL JOIN PRODUIT
WHERE RefC NOT IN (SELECT RefC
			FROM CLIENT,PRODUIT
			WHERE (RefC,TypeP) NOT IN (SELECT RefC,TypeP
							FROM COMMANDE NATURAL JOIN DETAIL NATURAL JOIN PRODUIT
							)
			)
;

-- 5. Références des commandes qui ne comportent que des produits
-- de types différents.

SELECT RefCom, COUNT(TypeP) as 'Nombre de Types ou de Ref de Produits'
FROM COMMANDE NATURAL JOIN DETAIL NATURAL JOIN PRODUIT
GROUP BY RefCom
HAVING COUNT(DISTINCT TypeP) = COUNT(RefP)
;

-- 6.  Villes où au moins un client n'a passé aucune commande 
-- ni en 2005, ni en 2006.
SELECT RefC
FROM COMMANDE
WHERE DateCom BETWEEN '2005-01-01' AND '2006-12-12'
;

SELECT DISTINCT Ville
FROM CLIENT
WHERE RefC NOT IN (SELECT RefC
			FROM COMMANDE
			WHERE DateCom BETWEEN '2005-01-01' AND '2006-12-12'
			)
;

-- 7. Villes où chaque client a passé au moins une commande en 2005 ou
-- 2006.

-- On soustrait à l'ensemble des villes l'ensemble des villes où au
-- moins un client n'a passé aucune commande ni en 2005, ni en 2006
-- (c'est-à-dire la table de la question précédente).

SELECT DISTINCT Ville
FROM CLIENT
WHERE Ville NOT IN (SELECT DISTINCT Ville
			FROM CLIENT
			WHERE RefC NOT IN (SELECT RefC
						FROM COMMANDE
						WHERE DateCom BETWEEN '2005-01-01' AND '2006-12-12'
						)
		)
;

-- 8. Paires de produits qui sont toujours commandés ensemble. 

-- Solution AVEC VARIABLES CORRELEES DANS REQUETES IMBRIQUEES : paires
-- de produits p1 et p2 telles que le nombre de commandes comportant à
-- la fois p1 et p2 est égal au nombre de commandes comportant p1 et
-- est égal aussi au nombre de commandes comportant p2


