
type Facteur = Int
type Exposant = Int
type Couple = (Facteur, Exposant)
type Decomposition = [Couple]


--
-- Partie vue en CM 03

pfactors :: Int -> [Int]
pfactors n = pfactors' n primes
   where pfactors' x (p:ps)
            | p > x          = []
            | mod x p == 0   = p : (pfactors' (div x p) (p:ps))
            | otherwise      = pfactors' x ps

-- juste pour info, voici le type de pfactors' :: Int -> [Int] -> [Int]

--
prep :: [Int] -> Decomposition
prep [] = []
prep (x:xs) = (x, (length (takeWhile (==x) (x:xs))) ) : (prep (dropWhile (==x) xs))

--
int2rep :: Int -> Decomposition
int2rep = prep . pfactors



-- 1.
--
-- rep2int, evalBis :: Decomposition -> Int

rep2int :: Decomposition -> Int
rep2int l = product[x^n | (x,n) <- l]

rep2intBis :: Decomposition -> Int
rep2intBis [] = 1
rep2intBis ((x,n):xs) = x^n * rep2intBis xs



-- 2.
--
estPremier :: Decomposition -> Bool
estPremier d
  |length d == 1 && snd(head d) == 1 = True
  |otherwise = False



-- 3.
--
pgcd, pgcdBis:: Decomposition -> Decomposition -> Decomposition

pgcd l1 l2 = [(x1,(min n1 n2)) |(x1,n1) <- l1 , (x2,n2) <- l2, x1 == x2]

pgcdBis [] _ = []
pgcdBis _ [] = []
pgcdBis a@((k1,d1):p1) b@((k2,d2):p2)
  | k1 == k2 = (k1,(min d1 d2)) : pgcdBis p1 p2
  | k1 < k2 = pgcdBis p1 b
  | otherwise = pgcdBis a p2



-- 4.
--
-- version récursive exactement sur le meme modèle que pgcd
ppcm :: Decomposition -> Decomposition -> Decomposition
ppcm [] p2 = p2
ppcm p1 [] = p1
ppcm a@((k1,d1):p1) b@((k2,d2):p2)
  | k1 == k2 = (k1,(max d1 d2)) : ppcm p1 p2
  | k1 < k2 = (k1,d1) : ppcm p1 b
  | otherwise = (k2,d2) : ppcm a p2



-- 5.
nbDiviseurs :: Decomposition -> Int
nbDiviseurs [] = 1
nbDiviseurs ((x,n):xs) = (n+1) * nbDiviseurs xs




-- 6.
--
op :: Couple -> [Int] -> [Int]
op (x,n) ys = [(x^i) * y | i <- [0..n], y <- ys]
--pas trop compris mais je vois comment l'utiliser pour diviseurs

diviseurs :: Decomposition -> [Int]
diviseurs [] = [1]
diviseurs (x:xs) = op x (diviseurs xs)


-- 7.
primes :: [Int]
primes =  sieve [2 .. ]
       where sieve (p:xs) = p : (sieve [x | x <- xs, mod x p > 0])

--Calcule les nombres premier
--primes ==> boucle infini qui renvoi tout les nombres premiers
--take 10 primes ==> renvoi les 10 premiers nombre premiers
--takeWhile (<= 100) primes ==> renvoi les nombres premiers <= 100
--length (takeWhile (<= 10000) primes) renvoi le nombre de nombres premier <= 10.000
