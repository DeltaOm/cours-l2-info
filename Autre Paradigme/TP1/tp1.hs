

-- 1. Les carres
--
square :: Int -> Int
square x = x * x

sumSquares :: Int -> Int -> Int
sumSquares x y = square x  + square y
-- pour sumSquaresMax -> penser à utiliser la fonction (min x y)

sumSquaresMax :: Int -> Int -> Int -> Int
sumSquaresMax x y z = sumSquares (max x y) (max (min x y) z)



-- 2. Conversion Celsius <--> Fahrenheit
--
c2f :: Float -> Float
c2f c = 9 / 5 * c + 32

f2c :: Float -> Float
f2c f = 5 / 9 * (f - 32)


-- 3. Remboursement selon tarifs kilometriques
--
-- trame à compléter

reductionRate :: Float -> Float
reductionRate nbPers
   | nbPers < 2  = 0
   | nbPers < 5  = 0.25
   | nbPers < 11 = 0.5
   | otherwise   = 0.75

travelExpenses :: Float -> Float -> Float
travelExpenses km nbPers = km * 0.52 * nbPers * (1 - reductionRate nbPers)



-- 4. Next Upper Case
--

decode :: Int -> Char
decode n = toEnum n

code :: Char -> Int
code c = fromEnum c

nextUpperCase :: Char -> Char
nextUpperCase c
    | code c == 90 = decode 65
    | otherwise = decode (code c + 1)





-- 5. Collatz function
--
collatz :: Int -> Int
collatz n
    | n == 2 = 1
    | even n = n `div` 2
    | otherwise = 3 * n + 1

-- 2 trames à compléter

nbCalls :: Int -> Int
nbCalls n
    | n == 1 = 0
    | otherwise = 1 + nbCalls (collatz n)


syracuse :: Int -> [Int]
syracuse n
    | n == 1    = []
    | otherwise = n : syracuse (collatz n)


--une autre définition possible de nbCalls en utilisant syracuse

nbCallsBis :: Int -> Int
nbCallsBis n = length (syracuse n)

--Si la conjecture était fausse, on aurait des fonctions infini

-- 6. Tous pairs
--

-- trame à compléter OU définir avec des if imbriqués OU en utilisant des gardes

allEven :: [Int] -> Bool 
allEven []     = True 
allEven (x:xs)
    | even x = allEven xs
    | otherwise = False 



-- 7. rire
--

-- ci dessous un ecriture en utilisant le pattern-matching sur les Int
--
-- laugh 0 = ""
-- laugh n = "HA " ++ laugh (n-1)

-- TODO définir laugh à l'aide d'un if ou bien en utilisant des gardes
laugh :: Int -> String 
laugh n
    | n > 0 = "HA " ++ laugh (n-1)
    | otherwise = ""


-- 8. Doubler cas general ++ cas particulier
--

-- Le cas general
doubleG :: [a] -> [a]
doubleG []     = []
doubleG (x:xs) = x:(x:(doubleG xs))

-- Mais, pour l'exo, il faut "aplatir" les String par concatenation
--
double :: [String] -> String
double [] = ""
double (x:xs) = x ++ " " ++ x ++ " " ++ double xs



-- 9.
--
sumSquaresEvenTest :: Int -> Int 
sumSquaresEvenTest x
    | x == 0 = 0
    | otherwise = square(x * 2) + sumSquaresEvenTest( x - 1 )

sumSquaresEven :: Int -> Int 
sumSquaresEven n = sum [square x | x <- [1..(n*2)], even x]


-- 10.
-- Tester mystery sur des listes d'entiers et conclure

mystery :: [Int] -> [Int]
mystery []     = []
mystery (x:xs) = mystery [y | y <- xs, y <= x]
                 ++  [x]
                 ++ mystery [y | y <- xs, y > x]

--Tri une liste d'entier dans l'ordre croissant