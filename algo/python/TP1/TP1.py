# -*- coding: utf-8 -*-
from math import *



# Question 1
# Le langage Python possède une structure proche des tableaux avec le type list.
# À ne pas confondre avec les listes chaînées
# qui ne sont pas directement implémentées en Python,
# et que l'on devra implémenter à la main (prochain TP).

T=[100,200,300,3.14,'bonjour']
print('\nQuestion 1\n')

# Afficher ce tableau en utilisant juste la fonction print
# puis sauter une ligne

# Afficher la longueur de cette liste, en utilisant la fonction len
# puis sauter une ligne

# Écrivez maintenant une fonction afficheTableau
# qui affiche les éléments du tableau, un sur chaque ligne.
# On utilisera la fonction len et une boucle for
# et la fonction range.
# Puis testez-la sur le tableau T précédent

#Décommenter la ligne suivante
#afficheTableau(T)

# Faire de même avec une boucle while, en appelant votre
# fontion afficheTableauTantQue

#Décommenter la ligne suivante
#afficheTableauTantQue(T)

# Question 2
print("\nQuestion 2\n")
# Écrivez une fonction remplitTableau qui remplit un tableau de n nombres en demandant des valeurs à l'utilisateur
# L'utiliser pour créer un tableau de 3 nombres 10,20,30 (pas des chaînes de caractères)

#Décommenter la ligne suivante
#V=remplitTableau(3)

# Question 3 écrivez une fonction insertionTriee qui insère un élément dans un tableau trié
#   de sorte qu'il reste trié (voir le CM 1), par la méthode naïve
# puis insérer successivement les nombres 23,5,47,12 dans le tableau V précédent

print("\nQuestion 3\n")

#Décommenter les lignes suivantes
#insertionTriee(V,23)
#insertionTriee(V,5)
#afficheTableau(V)
#insertionTriee(V,47)
#insertionTriee(V,12)
#afficheTableau(V)

# Question 4
# écrivez une fonction copieEnTriant qui recopie les éléments d'un tableau tabNonTrie
# dans un tableau copieTriee de sorte que les éléments de copieTriee
# soient triés par ordre croissant

print("\nQuestion 4\n")
#Décommenter les trois lignes suivantes
#print("Test : on créé le tableau tabNonTrie=[4,23,-8,7,2,12], on doit avoir après tri le tableau copieTrie=[-8,2,4,7,12,23]")
#tabNonTrie=[4,23,-8,7,2,12]
#afficheTableau(copieEnTriant(tabNonTrie))

# Question 5 Écrivez une fonction récursive rechDicho qui effectue une recherche dichotomique
# de la valeur x (voir le TD 1)
# On supposera que tab est un tableau trié par ordre croissant
# ajoutez un compteur que vous incrémenterez à chaque fois que vous recalculerez milieu
#vous retournerez (-1, compteur) lorsque l'élément recherché n'est pas dans le tableau et
#(i, compteur) lorsqu'il se trouve en position i
#ainsi si vous faites rep = rechDicho(tab,x), rep[0] donne la position et rep[1] le coût de la fonction.


print("\nQuestion 5\n")
tabTrie=[-8,-4,2,13,14,25]
#Décommenter les 6 lignes suivantes
#rechDicho(tabTrie,2)
#rechDicho(tabTrie,-7)
#rechDicho(tabT

# Question 6
# construisez un tableau tab avec 15 valeurs aléatoires entre 0 et 1000
# (importer la fonction randint du module random)
# et triez ses éléments dans un autre tableau copie
# Afficher les deux tableaux
# Générez aléatoirement plusieurs fois un indice entre 0 et 14 et calculez la complexité de la recherche de l'élément en position indice.
# Donnez la complexité minimale, maximale et celle en moyenne

print("\nQuestion 6\n")

#Décommenter la ligne suivante
#from random import randint






# Question 7
# Générer de même des tableaux triés de taille plus grande : 100,1000,peut-être 10000 (prudence)
# Générez aléatoirement plusieurs fois un indice entre 0 et taille-1 et calculez la complexité de la recherche de l'élément en position indice.
# Donnez la complexité en moyenne, comparez avec log_2(taille)

print("\nQuestion 7\n")
