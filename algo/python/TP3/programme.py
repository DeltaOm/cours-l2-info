# -*- coding: utf-8 -*-

###########################    Arbre Binaire    ###########################

#La classe Noeud permet de construire un nouveau noeud
#Par défaut, les sous-arbres gauche et droit sont égaux à None
class Noeud(object): 
   def __init__(self, element, gauche=None, droit=None): 
      self.valeur = element 
      self.gauche = gauche
      self.droit = droit

#Vous pourrez utiliser les deux fonctions suivantes    
def hauteur(arbre):
    """fonction qui calcul la hauteur d'un arbre binaire a"""
    if arbre == None: 
        return -1
    return 1+max(hauteur(arbre.gauche),hauteur(arbre.droit))

def dessinArbre(arbre,decalage=1):
   """un affichage planaire de l'arbre"""
   if arbre == None:
      return ""
   
   def affichageProfondeur(arbre,profondeur):
      if arbre == None:
         return " "
      ligne = affichageProfondeur(arbre.gauche,profondeur-1)
      if (profondeur == 0):
         ligne += str(arbre.valeur)
      else:
         ligne += " "
      return ligne + affichageProfondeur(arbre.droit,profondeur-1)

   chaine = ""
   for p in range(hauteur(arbre)+1):
      chaine += affichageProfondeur(arbre,p) + '\n'

   return chaine

#Vous testerez toutes les fonctions sur les deux arbres suivants
#ou d'autres arbres que vous définirez vous-mêmes

monArbre1 = Noeud(4,Noeud(1,Noeud(3,Noeud(6)),Noeud(9)),Noeud(3,Noeud(2,Noeud(5),None),Noeud(7)))
monArbre2 = Noeud(4,None,Noeud(3,Noeud(2,Noeud(1),Noeud(5)),None))

print("Premier arbre : \n" + dessinArbre(monArbre1)+"\n")
print("Second arbre : \n" + dessinArbre(monArbre2)+"\n")

#Exercice 1. Parcours en profondeur sur un arbre binaire
#1.a Complétez cette fonction qui retourne la liste des valeurs des noeuds d'un arbre binaire dans l'ordre préfixe.
#Rappel: lst1 + lst2 permet d'obtenir une liste qui est la concaténation des deux listes lst1 et lst2.
def ordrePrefixe(arbre):
    return []

#1.b Complétez cette fonction qui retourne la liste des valeurs des noeuds d'un arbre binaire dans l'ordre infixe.
def ordreInfixe(arbre):
    return []
    
#1.c Complétez cette fonction qui retourne la liste des valeurs des noeuds d'un arbre binaire dans l'ordre suffixe.
def ordreSuffixe(arbre):
    return []

#Exercice 2. Complétez cette fonction qui retourne le nombre de noeuds internes d'un arbre  binaire.
def nombreNoeud(arbre):
    return 0

#Exercice 3. Complétez cette fonction estDegenere qui détermine si un arbre binaire est dégénéré
#On rappelle qu'un arbre est dégénéré lorsque tout noeud a au plus un fils.
def estDegenere(arbre):
    return False

#Exercice 4. Complétez cette fonction noeudsNiveau(arbre,k) qui retourne une liste des noeuds du niveau k
#Indication : pendant les appels récursifs k diminue et correspond à la longueur de chemin à parcourir
#pour atteindre un noeud de profondeur k, nous avons trois cas :
#cas 1) arbre = None et on ne fait rien
#cas 2) k = 0 le noeud est de profondeur k et on l'ajoute à la liste
#cas 3) k > 0 des noeuds de profondeur k peuvent se trouver dans les sous-arbres gauche et droit
#on effectue deux appels récursifs sur les deux sous-arbres et on fusionne les 2 liste obtenues.

def noeudsNiveau(arbre,k):
    return []
    
#Exercice 5. Complétez cette fonction noeudsLargeur(arbre) qui retourne la liste des valeurs des noeuds d'un arbre binaire
#en suivant le parcours en largeur
#Vous utiliserez impérativement les fonctions hauteur et noeudsNiveau
def noeudsLargeur(arbre):
    return []

#Exercice 6. On rappelle que la longeur de cheminement d'un arbre binaire est
#la somme des profondeurs de tous les noeuds de cet arbre.
#Complétez cette fonction longueurCheminement(arbre,prof=0) qui calcule la longueur de cheminement
#d'un arbre binaire "arbre".
#On appelle la fonction sur l'arbre "arbre" avec l'instruction longueurCheminement(arbre) qui
#met la variable prof à 0 (la racine est de profondeur 0)
#nous avons ensuite deux cas
#cas 1) arbre = None, déterminez ce que la fonction doit retourner
#cas 2) arbre != None, on retourne le profondeur du noeud sur lequel on se trouve +
#la longueur de cheminement des deux sous-arbres gauche et droit
def longueurCheminement(arbre,prof=0):
    return 0

#Exercice 7. Un chemin décroissant dans un arbre est un chemin partant de la racine
#dans lequel la valeur des noeuds décroît au fur et à mesure qu'on descend dans l'arbre.
#Un chemin décroissant ne s'arrête pas forcément à une feuille.
#Par exemple, dans monArbre1, il y a deux chemins décroissants :
# 4-3-2 et 4-1.
# 7.a)  Complétez cette fonction qui renvoie la longueur du PLUS LONG chemin décroissant dans un arbre
def longueurCheminDecroissantMax(arbre,prof=0):
    return 0
    
# 7.b) Complétez cette fonction qui donne la liste des valeurs du plus long chemin chemin décroissant dans un arbre
# (si deux chemin decroissant max sont de longueur égales, on ira à gauche en priorité)
def cheminDecroissantMax(arbre):
    return []



