#include <stdio.h>
#include <stdlib.h>



/***************** Définition de la structure **************************/

/*  Définition de la structure de noeud  */
struct noeud {
    int valeur;
    struct noeud *suivant;
};

/*  Définition du type liste qui est un pointeur sur noeud  */
typedef struct noeud *liste;


/***************** Fonctions utiles **************************/

/*  Fonction affichant les valeurs des noeuds d'une liste L  */
void afficheListe(liste L) {
    if (L == NULL) printf("[X]\n");
    while (L != NULL) {
        printf("[%d,", L->valeur);
        L = L->suivant;
        if (L == NULL) {
            printf("X]\n");
        } else {
            printf("-]-->");
        }
    }
}

/*  nouveauNoeud(x) crée une nouvelle liste contenant un noeud
 * avec la valeur x et le suivant égal à NULL  */
liste nouveauNoeud(int x) {
    liste L = malloc(sizeof(struct noeud));
    L->valeur = x;
    L->suivant = NULL;
    return L;
}


/*  Fonction insérant un nouveau noeud de valeur x au début de la liste L  */
liste insereDebut(liste L, int x) {
    liste tmp = nouveauNoeud(x);
    if (L != NULL) {
        tmp->suivant = L;
    }
    return tmp;
}

/******************** Début des questions ********************/


/*  Question 1. Écrivez une fonction qui étant donnée un entier positif
 *  "taille" renvoie une liste chaînée de taille "taille" dont la valeur
 *	de chaque élément est 0.
 *  On pourra utiliser la fonction "insereDebut" donnée plus haut. */

liste listeZeros(size_t taille);

liste listeZeros(size_t taille) {
    liste l;
    if (taille == 0) {
        return NULL;
    }
    l = insereDebut(NULL, 0);
    liste tmp = l;
    for (int i = 1; i < taille; i++) {
        l->suivant = nouveauNoeud(0);
        l = l->suivant;
    }
    return tmp;
}


/* Question 2. Écrire une fonction qui calcule le nombre de noeuds
 * d'une liste chaînée L */

size_t nombreNoeuds(liste L);

size_t nombreNoeuds(liste L) {
    size_t size = 0;
    while (L != NULL) {
        L = L->suivant;
        size++;
    }
    return size;
}



/* Question 3. On veut écrire une fonction qui insère un nouveau noeud
 * de valeur x à la fin d'une liste L.
 * On renverra la liste ainsi obtenue.  */


/* Question 3a. Écrire une fonction itérative de cette fonction. */

liste insererFinIt(liste L, int x);

liste insererFinIt(liste L, int x) {
    liste l = L;
    if (L == NULL) {
        return nouveauNoeud(x);
    }
    while (L->suivant != NULL) {
        L = L->suivant;
    }
    L->suivant = nouveauNoeud(x);
    return l;
}

/* Question 3b. Écrire une fonction récursive de cette fonction. */

liste insererFinRec(liste L, int x);

liste insererFinRec(liste L, int x) {
    if (L == NULL) {
        L = nouveauNoeud(x);
    } else {
        insererFinIt(L->suivant, x);
    }
    return L;
}


/* Question 4. Écrire une fonction retournant l'adresse (le pointeur
 * sur le noeud, autrement dit une "liste") du premier noeud contenant
 * la valeur x.
 * On retournera NULL lorsque la liste ne contient pas la valeur x.  */

liste recherche(liste L, int x);

liste recherche(liste L, int x) {
    while (L != NULL) {
        if (L->valeur == x) {
            return L;
        }
        L = L->suivant;
    }
    return NULL;
}


/* La fonction précédente permet de faire fonctionner la fonction suivante :
 * Elle insère un nouveau noeud avec la valeur y après le premier noeud
 * contenant la valeur x. S'il n'y a pas de noeud contenant la valeur x,
 * cette fonction ne fait rien.  */

//A decommenter

liste insereApres(liste L, int x, int y) {
    liste tmp = recherche(L, x);
    if (tmp != NULL) {
        liste tmp2 = tmp->suivant;
        tmp->suivant = nouveauNoeud(y);
        tmp->suivant->suivant = tmp2;
    }
    return L;
}

/* Question 5. S'inspirer de la fonction précédente pour écrire une
 * fonction insérant un nouveau noeud avec la valeur y *avant* le
 * premier noeud contenant la valeur x.
 * S'il n'y a pas de noeud contenant la valeur x, cette fonction ne fait
 * rien.
 * Astuce : on reprend la fonction insereApres et on échange les
 * valeurs x et y  */

liste insereAvant(liste L, int x, int y);

liste insereAvant(liste L, int x, int y) {
    liste tmp = recherche(L, x);
    if (tmp == NULL) {
        return L;
    } else if (L == tmp) {
        L = insereDebut(L, y);
        return L;
    }
    liste l = L;
    while (L->suivant != tmp) {
        L = L->suivant;
    }
    liste ntmp = nouveauNoeud(y);
    L->suivant = ntmp;
    L = L->suivant;
    L->suivant = tmp;
    return l;
}


/* Question 6. Écrire une fonction retourne qui prend en paramètre
 * une liste L et qui *modifie* L de sorte que ses élements
 * apparaissant dans l'ordre inversé.
 * On renverra la liste ainsi modifiée (pointant sur le dernier noeud
 * de L).
 * Vous ne devez pas changer la valeur des noeuds, seulement réordonner
 * les pointeurs.
 * Exemple : Si L est la liste chaînée ->[1,-]-->[2,-]-->[3,X] alors on doit
 * renvoyer la liste [1,X]  [2,|]   [3,|]<--
 *                      ↖______| ↖_____|     */

liste retourne(liste L);

liste retourne(liste L) {
    if (L == NULL) {
        return L;
    } else if (L->suivant == NULL) {
        return L;
    }
    liste actual = L->suivant, last = L, next = actual->suivant;
    while (actual->suivant != NULL) {
        actual->suivant = last;
        last = actual;
        actual = next;
        next = next->suivant;
    }
    actual->suivant = last;
    L->suivant = NULL;
    return actual;
}


/* Question 7. Écrire une fonction qui prend en paramètre une liste chaînée
 * L et qui renvoie une *nouvelle* liste chaînée de même longueur que L
 * dans laquelle l'élément en position i prend pour valeur
 * la valeur maximale que prend un noeud de L situé en position i ou
 * après la position i.
 * Par exemple, si on exécute la fonction sur la liste L
 * ->[7,-]-->[20,-]-->[11,-]-->[6,-]-->[15,-]-->[12,-]-->[10,X]
 * on doit renvoyer
 * ->[20,-]-->[20,-]-->[15,-]-->[15,-]-->[15,-]-->[12,-]-->[10,X]
 * En effet, la valeur maximale de la liste L est 20, donc le premier
 * élément de la nouvelle liste a pour valeur 20 ;
 * la valeur maximale de la liste L à partir de la deuxième position est
 * toujours 20, donc le deuxième élément de la nouvelle liste a aussi
 * valeur 20 ;
 * par contre, la valeur maximale de la liste L à partir de la troisième
 * position est 15 ; donc le troisième élément de la nouvelle liste aura
 * pour valeur 15 ;
 * etc.
 * */

liste listeMaximums(liste L);

liste listeMaximums(liste L) {
    if (L == NULL) {
        return L;
    }
    liste actual = L, last = L;
    while (actual->suivant != NULL) {
        actual = actual->suivant;
        if (last->valeur < actual->valeur) {
            last->valeur = actual->valeur;
        }
        last = actual;
    }
    liste stock = L, next = L->suivant;
    for (int i = 0; i < nombreNoeuds(L) - 1; i++) {
        if (stock -> valeur < next -> valeur) {
            listeMaximums(L);
        }
        stock = stock -> suivant;
        next = next -> suivant;
    }
    return L;
}

/***************** Pour tester vos fonctions : *********/
/* (ne pas touchez aux "ifndef" et "endif") */
#ifndef EVAL

int main() {

    liste L1;
    L1 = nouveauNoeud(10);
    L1->suivant = nouveauNoeud(7);
    L1->suivant->suivant = nouveauNoeud(18);
    printf("Voici L1 : ");
    afficheListe(L1);

    // Question 1
    printf("\nQuestion 1\n");
    liste l2 = listeZeros(3);
    printf("Voici L2 : ");
    afficheListe(l2);

    // Question 2
    printf("\nQuestion 2\n");
    printf("taille L1 = %ld\n", nombreNoeuds(L1));

    // Question 3
    printf("\nQuestion 3\n");
    afficheListe(insererFinIt(L1, 2));
    afficheListe(insererFinRec(L1, 2));

    // Question 4
    printf("\nQuestion 4\n");
    afficheListe(recherche(L1, 18));

    // Question 5
    printf("\nQuestion 5\n");
    afficheListe(insereAvant(L1, 7, 80));

    // Question 6
    /*
    printf("\nQuestion 6\n");
    afficheListe(retourne(L1));*/
    // Question 7
    printf("\nQuestion 7\n");
    afficheListe(listeMaximums(L1));

    return EXIT_SUCCESS;
}

#endif
