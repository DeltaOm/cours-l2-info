
public class Point{

    private double x;
    private double y;
    private static int nInstances = 0;

    public Point(double x, double y){
        this.x = x;
        this.y = y;
        nInstances += 1;
    }

    public Point(){
        new Point(0,0);
    }

    public Point(Point point){
      new Point(point.getX(), point.getY());
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public static int getNInstances(){
        return nInstances;
    }

    public void setX(double x){
        this.x = x;
    }

    public void setY(double y){
        this.y = y;
    }

    public String toString(){
        return "("+this.getX()+","+this.getY()+")";
    }

    public void translation(double dx, double dy){
      setX(x+dx);
      setY(y+dy);
    }

    public void homothetie(Point centre, double k){
      setX(centre.getX() * (1 - k) + x * k);
      setY(centre.getY() * (1 - k) + y * k);
    }

    public void homothetie(double centreX, double centreY, double k){
      homothetie(new Point(centreX,centreY), k);
    }
}
