public class Polygone{

  private Point[] points;

  /**
   * Constructeur de Polygone, mettre les points dans un ordre horaire, sinon calcule d'air cassé
   * @method Polygone
   * @param  points    Liste des points ordonnées dans un ordre horaire
   */
  public Polygone(Point[] points){
    this.points = points;
  }

  public Point[] getPoints(){
    return points;
  }

  public void setPoints(Point[] points){
    this.points = points;
  }
}
