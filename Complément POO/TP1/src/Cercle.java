public class Cercle{

  private Point centre;
  private double rayon;

  public Cercle(Point c, double r){
    centre = new Point(c);
    rayon = r;
  }

  public Point getCentre(){
    return centre;
  }

  public double getRayon(){
    return rayon;
  }

  public void setCentre(Point c){
    centre = new Point(c);
  }

  public void setRayon(double r){
    rayon = r;
  }

  public void translation(double dx, double dy){
    getCentre().translation(dx,dy);
  }

  public void homothetie(Point centre, double k){
    getCentre().homothetie(centre,k);
    rayon *= k;
  }

  public void homothetie(double centreX, double centreY, double k){
    homothetie(new Point(centreX,centreY), k);
  }

  public String toString(){
    return "centre="+getCentre()+", rayon="+getRayon();
  }
}
