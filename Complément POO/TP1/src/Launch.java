public class Launch{

    public static void main(String[] args) {
      System.out.println(Point.getNInstances());
      System.out.println("création du premier point");
      Point p1 = new Point();

      System.out.println(Point.getNInstances());
      System.out.println("création du second point");
      Point p2 = new Point();

      System.out.println(Point.getNInstances());
      System.out.println("création du troisième point");
      Point p3 = new Point();

      System.out.println(Point.getNInstances());

      System.out.println("Test de la translation du point (5,2) de dx = 1 et dy = 1");
      Point translation1 = new Point(5,2);
      translation1.translation(1,1);

      if(translation1.toString().equals("(6.0,3.0)")){
        System.out.println("Success");
      }
      else{
        System.out.println("Error translation(double,double)");
      }

      System.out.println("Test de l'homothetie d'un point (2,2) de centre (1,1) et de rapport 3");
      System.out.println("Test de la fonction homothetie(Point,double)");
      Point homothetie1 = new Point(2,2);
      homothetie1.homothetie(new Point(1,1),3);

      if(homothetie1.toString().equals("(4.0,4.0)")){
        System.out.println("Success");
      }
      else{
        System.out.println("Error homothetie(Point,double)");
      }

      System.out.println("Test de la fonction homothetie(double,double,double)");
      Point homothetie2 = new Point(2,2);
      homothetie2.homothetie(1,1,3);

      if(homothetie1.toString().equals("(4.0,4.0)")){
        System.out.println("Success");
      }
      else{
        System.out.println("Error homothetie(double,double,double)");
      }

      System.out.println("--------------------------\n");
      System.out.println("Test de la translation d'un cercle de centre (5,2), de rayon 2, de dx = 1 et dy = 1");

      Cercle translationCercle1 = new Cercle(new Point(5,2),2);
      translationCercle1.translation(1,1);

      if(translationCercle1.toString().equals("centre=(6.0,3.0), rayon=2.0")){
        System.out.println("Success\n");
      }
      else{
        System.out.println("Error translation(double,double)\n");
      }

      System.out.println("Test de homothetie(Point,double), cercle de centre (2,2), rayon 4, d'homothetie de centre (1,1) et de rapport 3");
      Cercle homothetieCercle1 = new Cercle(new Point(2,2),4);
      homothetieCercle1.homothetie(new Point(1,1) , 3);

      if(homothetieCercle1.toString().equals("centre=(4.0,4.0), rayon=12.0")){
        System.out.println("Success\n");
      }
      else{
        System.out.println("Error homothetie(Point,double)\n");
      }

      System.out.println("Test de homothetie(Point,double), cercle de centre (2,2), rayon 4, d'homothetie de centre (1,1) et de rapport 3");
      Cercle homothetieCercle2 = new Cercle(new Point(2,2),4);
      homothetieCercle2.homothetie(1, 1 , 3);

      if(homothetieCercle2.toString().equals("centre=(4.0,4.0), rayon=12.0")){
        System.out.println("Success\n");
      }
      else{
        System.out.println("Error homothetie(Point,double)\n");
      }
    }
}
