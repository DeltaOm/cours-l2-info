from random import randint
 
 
bordure='01111110'
chaine_envoyee = "1111011100001011010"

def parity_bit(message_bin):
    return str(message_bin.count('1')%2)

def double_parity(message_bin,n):
    LRC = ""
    VRC = ""
    triche = [""]*n #à finir
    i = 0
    while i < len(message_bin):
        message_coupee = message_bin[i:i+n]
        LRC += parity_bit(message_coupee)
        for i in range (n):
            triche[i] = triche[i]+message_coupee[i]
        i += n
    for i in triche:
        VRC += parity_bit(i)
    
    return LRC + VRC


print(double_parity("101001101101",4))
# Cette fonction traduit un message en une trame/sequence de bits. Cette fonction est maintenant simple, mais elle est insuffisante (elle ne permet pas de trouver le debut du message par exemple) le but de ce TP est de la completer.
def addIdentification(src, dst):   
    return format(dst, '04b')+format(src, '04b')
    
def WhoIsTheSender(trame):
    return int(trame[4:8],2)

def IsAdressToMe(trame, dst):
    dst_bin = format(dst, '04b')
    dst_trame_bin = trame[0:4]
    return dst_bin == dst_trame_bin

def SignalSwapError(signal,n):
    if signal[n] == '+':
        signalSwap = signal[0:n]+'-'+signal[n+1:]
    else:
        signalSwap = signal[0:n]+'+'+signal[n+1:]
    return signalSwap


def couche2(message_from_sender):
    result =""
    for character in message_from_sender:
        result += bin(ord(character)).replace('b','')
    #taille en décimal
    taille = len(result)
    # coder la taille en 8 bits
    taille_bin = format(taille, '08b')
    return bordure+addIdentification(1,3)+taille_bin+result+parity_bit(result)

 
# Cette fonction traduit une sequence de bits en un message. Cette fonction est maintenant simple, mais elle est insuffisante (elle ne permet pas de trouver le debut du message par exemple) le but de ce TP est de la completer.
def couche2R(bits_from_sender):
    result = ""
    i = 0
    # chercher la bordure
    trouvee = False
    while i < len(bits_from_sender):
        # récupérer le bloc de 8
        bloc8bits = bits_from_sender[i:i+8]
        # comparer le bloc avec la bordure
        if bloc8bits == bordure:
            trouvee = True
            i+=8
            break
        i+=1
    # la bordure est trouvee
    if trouvee :
        # vérifier l'adresse
        trame = bits_from_sender[i:]
        print("emetteur: "+str(WhoIsTheSender(trame)))
        if IsAdressToMe(trame, 3):
            # sauter les adresses
            i+=8
            # récupérer la taille en 8 bits
            taille_bin = bits_from_sender[i:i+8]
            # retrouver la taille en décimal
            taille = int(taille_bin,2)
            # passer aux données (champs suivant)
            i+=8
            taille+=i
            #vérification du bit de parité
            #Ne remarque que si le nombre d'erreur est impaire
            if parity_bit(bits_from_sender[i:taille]) != bits_from_sender[taille]:
                return "Erreur de bit de parité"
            #traduction du message
            while i < taille:
                bin_character = bits_from_sender[i:i+8]
                character = chr(int(bin_character,2))
                result += character
                i += 8
        else:
            return "message ignorée"
    return result
 
#cette fonction represente la tradution niveau 1 faite par le codage NRZ
def couche1(trame_de_bits):
    return trame_de_bits.replace("0","-").replace("1","+")
 
#cette fonction represente la tradution faite par le codage NRZ
def couche1R(trame_de_bits):
    return trame_de_bits.replace("-","0").replace("+","1")
 
 
#Cette fonction simule l'état du canal (ex : un fil electrique).
#Un canal est présent avant que l'émetteur ne veuille envoyer le message. Il contient initialement du bruit: le recepteur etait a l'ecoute avant que l'emetteur n'envoie son message.
#Il contient aussi un bruit final, car le recepteur sera aussi a l'ecoute apres l'envoi du message.
#Le defi de la couche 2 est de permettre au recepteur de determiner ou commence et ou termine le message envoye.
def transmission(signal_from_sender):
#NE PAS MODIFIER CETTE FONCTION : elle simule le "monde reel", sur lequel vous n'avez pas prise. Le travail de ce TP consiste a modifier couche2 et couche2R
 
    print("Nombre de signaux necessaires pour envoyer le message:"+ str(len(signal_from_sender)))
    
    #SignalSwapError
    signal_from_sender = SignalSwapError(signal_from_sender,28)#Une erreur
    signal_from_sender = SignalSwapError(signal_from_sender,29)#deux erreurs
    signal_from_sender = SignalSwapError(signal_from_sender,30)#trois erreurs
    #bruit initial
    for x in range (0,randint(4, 10)):
        if randint(0,1) == 1:
            signal_from_sender = "-" + signal_from_sender
        else:
            signal_from_sender = "+" + signal_from_sender
 
    #bruit final
    for x in range (0,randint(4, 10)):
        if randint(0,1) == 1:
            signal_from_sender = signal_from_sender + "-"
        else:
            signal_from_sender = signal_from_sender + "+"
 
    return signal_from_sender
 
 
def emission(message_from_sender):
    print("message a envoyer:"+message_from_sender)
    trame_de_bits_envoyee = couche2(message_from_sender)
    print("trame a envoyer:"+trame_de_bits_envoyee)
    signal_envoye = couche1(trame_de_bits_envoyee)
    print("signal envoye:"+signal_envoye)
    return signal_envoye
 
def reception(signal_sur_canal):
    print("signal entendu, avec le bruit:"+signal_sur_canal)
    bits_recus = couche1R(signal_sur_canal)
    print("bits recus par le recepteur:"+bits_recus)
    message_recu = couche2R(bits_recus)
    print("message recu par le recepteur:"+message_recu)
    return message_recu
 
 
def simulation(message_from_sender):
    signal_envoye = emission(message_from_sender)
    signal_sur_canal = transmission(signal_envoye)
    return reception(signal_sur_canal)
 
print(simulation("oooiii")) 
