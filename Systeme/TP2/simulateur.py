from random import randint
 
# Cette fonction traduit un message en une trame/sequence de bits. Cette fonction est maintenant simple, mais elle est insuffisante (elle ne permet pas de trouver le debut du message par exemple) le but de ce TP est de la completer.
def couche2(message_from_sender):
  #result =""
  header = "01111110" #bordure de début de message
  result = header
  
  #Taille du message en bits, format 8 bits
  taille = len(message_from_sender) *8
  binTaille = bin(taille).replace('b','')
  while(len(binTaille) < 8):
      binTaille = '0'+binTaille

  #ajout de binTaille après le header  
  result += binTaille
  for character in message_from_sender:
    result += bin(ord(character)).replace('b','')
  return result

 
# Cette fonction traduit une sequence de bits en un message. Cette fonction est maintenant simple, mais elle est insuffisante (elle ne permet pas de trouver le debut du message par exemple) le but de ce TP est de la completer.
def couche2R(bits_from_sender):
        result = ""
        header = "01111110"
        #cherche l'index du début du message +len(header) pour sauter
        debut = bits_from_sender.find(header)+len(header);
        taille = int(int(bits_from_sender[debut:debut+8],2))  
        i = debut + 8
        j = 0
        print("test")
        print(bits_from_sender.find(header))
        print(taille)
        print(bits_from_sender[debut:debut+8])
        print(bits_from_sender[debut-8:debut])
        while j < taille:
                bin_character = bits_from_sender[i+j:i+j+8]
                character = chr(int(bin_character,2))
                result += character
                j += 8
        return result 
 
#cette fonction represente la tradution niveau 1 faite par le codage NRZ
def couche1(trame_de_bits):
    return trame_de_bits.replace("0","-").replace("1","+")
 
#cette fonction represente la tradution faite par le codage NRZ
def couche1R(trame_de_bits):
    return trame_de_bits.replace("-","0").replace("+","1")

#cette fonction represente la tradution niveau 1 faite par le codage NRZI
def couche1NRZI(bits):
    signal = ""
    prec = "-"
    for b in bits:
        if b == "0":
            signal += prec
        elif b == "1" :
            if prec == "+" :
                prec = '-'
            else :
                prec = "+"
            signal += prec
        else :
            print("Erreur")
    return signal

#cette fonction represente la tradution faite par le codage NRZ
def couche1NRZIR(signal):
    bits = ""
    prec = "-"
    for s in signal:
        if s != "+" and s != "-":
            print("Erreur")
        if prec == s:
            bits += "0"
        else:
            bits += "1"
            prec = s
    return bits
 
#Cette fonction simule l'état du canal (ex : un fil electrique).
#Un canal est présent avant que l'émetteur ne veuille envoyer le message. Il contient initialement du bruit: le recepteur etait a l'ecoute avant que l'emetteur n'envoie son message. 
#Il contient aussi un bruit final, car le recepteur sera aussi a l'ecoute apres l'envoi du message. 
#Le defi de la couche 2 est de permettre au recepteur de determiner ou commence et ou termine le message envoye.
def transmission(signal_from_sender):
#NE PAS MODIFIER CETTE FONCTION : elle simule le "monde reel", sur lequel vous n'avez pas prise. Le travail de ce TP consiste a modifier couche2 et couche2R
 
    print("Nombre de signaux necessaires pour envoyer le message:"+ str(len(signal_from_sender)))
    #bruit initial
    for x in range (0,randint(4, 10)):
        if randint(0,1) == 1:
            signal_from_sender = "-" + signal_from_sender
        else:
            signal_from_sender = "+" + signal_from_sender
 
    #bruit final
    for x in range (0,randint(4, 10)):
        if randint(0,1) == 1:
            signal_from_sender = signal_from_sender + "-"
        else:
            signal_from_sender = signal_from_sender + "+"
            
    return signal_from_sender
 
 
def emission(message_from_sender):
    print("message a envoyer:"+message_from_sender)
    trame_de_bits_envoyee = couche2(message_from_sender)
    print("trame a envoyer:"+trame_de_bits_envoyee)
    signal_envoye = couche1NRZI(trame_de_bits_envoyee)
    print("signal envoye:"+signal_envoye)
    return signal_envoye
 
def reception(signal_sur_canal):
    print("signal entendu, avec le bruit:"+signal_sur_canal)
    bits_recus = couche1NRZIR(signal_sur_canal)
    print("bits recus par le recepteur:"+bits_recus)
    message_recu = couche2R(bits_recus)
    print("message recu par le recepteur:"+message_recu)
    return message_recu
 
 
def simulation(message_from_sender):
    signal_envoye = emission(message_from_sender)
    signal_sur_canal = transmission(signal_envoye)
    return reception(signal_sur_canal)
 
print(simulation("oooiii"))
