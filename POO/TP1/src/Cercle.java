package geometry;

public class Cercle{
  private Position milieu;
  private int rayon;

  public Cercle(Position milieu, int rayon){
    this.milieu = milieu;
    this.rayon = rayon;
  }

  public String getRepresentation(){
    String ret = "Milieu :"+this.milieu.getRepresentation()+", Rayon : "+this.rayon;
    return ret;
  }

  public void translate(int x, int y){
    this.milieu.translate(x,y);
  }

  public int diametre(){
    return 2*this.rayon;
  }

  public double perimetre(){
    return 2*Math.PI*this.rayon;
  }

  public double surface(){
    return Math.PI*Math.pow(this.rayon,2.0);
  }
}
