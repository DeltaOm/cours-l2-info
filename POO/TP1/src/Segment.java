package geometry;

public class Segment{
  private Position debut;
  private Position fin;

  public Segment(Position debut, Position fin){
    this.debut = debut;
    this.fin = fin;
  }

  public String getRepresentation(){
    String ret = "Départ : "+this.debut.getRepresentation()+" ; Fin : "+this.fin.getRepresentation();
    return ret;
  }

  public double length(){
    double ret = Math.sqrt(Math.pow((this.fin.getX()-this.debut.getX()),2.0)+Math.pow((this.fin.getY()-this.debut.getY()),2.0));
    return ret;
  }

  public Position milieu(){
    int milieuX = (this.debut.getX() + this.fin.getX()) / 2;
    int milieuY = (this.debut.getY()+this.fin.getY()) / 2;
    Position ret = new Position(milieuX,milieuY);
    return ret;
  }

  public Segment symmetricX(){
    Position debutSym = this.debut.symmetricX();
    Position finSym = this.fin.symmetricX();
    return new Segment(debutSym, finSym);
  }

  public Segment symmetricY(){
    Position debutSym = this.debut.symmetricY();
    Position finSym = this.fin.symmetricY();
    return new Segment(debutSym, finSym);
  }

  public void translate(int x, int y){
    this.debut.translate(x,y);
    this.fin.translate(x,y);
  }
}
