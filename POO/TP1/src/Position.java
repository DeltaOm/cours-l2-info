package geometry;

public class Position{
  private int x;
  private int y;

  public Position(int x, int y){
    this.x = x;
    this.y = y;
  }

  public Position(){
    this(0,0);
  }

  public int getX(){
    return this.x;
  }

  public int getY(){
    return this.y;
  }

  public String getRepresentation(){
    return "("+this.getX()+","+this.getY()+")";
  }

  public Position symmetricX(){
    Position sym = new Position(this.getX(), -this.getY());
    return sym;
  }

  public Position symmetricY(){
    Position sym = new Position(-this.getX(), this.getY());
    return sym;
  }

  public void translate(int deltaX, int deltaY){
    this.x += deltaX;
    this.y += deltaY;
  }
}
