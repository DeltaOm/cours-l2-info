package geometry;

public class Rectangle{

  private Position point1;
  private Position point2;

  public Rectangle(Position point1, Position point2){
    this.point1 = point1;
    this.point2 = point2;
  }

  public String getRepresentation(){
    String ret = "Rectangle défini par 2 Positions, Positions 1 :"+this.point1.getRepresentation()+", Positions 2 : "+this.point2.getRepresentation();
    return ret;
  }

  public void translate(int x, int y){
    this.point1.translate(x,y);
    this.point2.translate(x,y);
  }

  public int perimetre(){
    int ret = Math.abs(this.point1.getX()-this.point2.getX())*2 + Math.abs(this.point1.getY()-this.point2.getY())*2;
    return ret;
  }

  public int surface(){
    int ret = Math.abs(this.point1.getX()-this.point2.getX()) + Math.abs(this.point1.getY()-this.point2.getY());
    return ret;
  }

}
