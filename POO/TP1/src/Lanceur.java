import geometry.*;
import geometrytests.*;

public class Lanceur{
  public static void main(String[] args) {
    Position pos = new Position(3,4);
    System.out.println(pos.getRepresentation());

    System.out.println("\nTest de la symétrie");
    Position symetrie = pos.symmetricX();
    System.out.println(symetrie.getRepresentation());

    System.out.println("\nTest de la translation, vecteur (4,2)");
    System.out.println("On espère obtenir le résultat 7 pour X, 6 pour Y");
    pos.translate(4,2);
    System.out.println("\n"+pos.getRepresentation());

    System.out.println("\nLancement des méthodes de test de la class Position");

    boolean ok = true;
    PositionTests positionTester = new PositionTests();
    ok = ok && positionTester.testGetX();
    ok = ok && positionTester.testGetY();
    ok = ok && positionTester.testSymmetricX();
    ok = ok && positionTester.testTranslate();
    System.out.println(ok ? "All tests passed" : "At least one test failed");

    System.out.println("\nSegments");
    Segment seg1 = new Segment(new Position(3,4),new Position(7,7));

    System.out.println("Affichage de la représentation du segment ayant comme départ (3,4) et fin (7,7)");
    System.out.println(seg1.getRepresentation());

    System.out.println("Affichage de la longueur du segment: résultat = 5.0");
    System.out.println(seg1.length());

    System.out.println("\nLancement des méthodes de test de la class Segment");

    ok = true;
    SegmentTests segmentTester = new SegmentTests();
    ok = ok && segmentTester.testLength();
    System.out.println(ok ? "All tests passed" : "At least one test failed");

    //=====================================================================================================================================================//
    System.out.println("\nExercices complémentaire");

    System.out.println("\n Milieu du segment ayant comme début (3,4), fin (7,7) : résultat espéré (5, 5) //partie entière pas de double");
    Position milieu = seg1.milieu();
    System.out.println(milieu.getRepresentation());

    System.out.println("\nSymétrie d'un segment par l'axe X, on attend l'obtention du segment debut (3,-4), fin (7,-7)");
    Segment segSymX = seg1.symmetricX();
    System.out.println(segSymX.getRepresentation());

    System.out.println("\nSymétrie d'un segment par l'axe Y, on attend l'obtention du segment debut (-3,4), fin (-7,7)");
    Segment segSymY = seg1.symmetricY();
    System.out.println(segSymY.getRepresentation());

    System.out.println("\nTranslation du segment de +5 sur X et -3 sur Y, on attend le résultat : début (8,1), fin (12,4)");
    Segment seg2 = new Segment(new Position(3,4),new Position(7,7));
    seg2.translate(5,-3);
    System.out.println(seg2.getRepresentation());

    System.out.println("\nCercle :");
    System.out.println("\n Représentation d'un Cercle de point milieu (0,0) et de rayon 3");
    Cercle cercle = new Cercle(new Position(0,0),3);
    System.out.println(cercle.getRepresentation());

    System.out.println("\n translation du cercle de +5 sur X et -3 sur Y : résultat attendu milieu (5,-3) rayon 3");
    cercle.translate(5,-3);
    System.out.println(cercle.getRepresentation());

    System.out.println("\n Périmètre du cercle : résultat attendu ~= 18.849");
    System.out.println(cercle.perimetre());

    System.out.println("\n Surface du cercle : résultat attendu ~= 28.27");
    System.out.println(cercle.surface());

    System.out.println("\nrecta,gle :");
    System.out.println("\n Représentation d'un Rectangle de point 1 (0,0) et de point 2 (1,1)");
    Rectangle rectangle = new Rectangle(new Position(0,0),new Position(1,1));
    System.out.println(rectangle.getRepresentation());

    System.out.println("\n translation du rectangle de +5 sur X et -3 sur Y : résultat attendu point 1 (5,-3) et de point 2 (6,-2)");
    rectangle.translate(5,-3);
    System.out.println(rectangle.getRepresentation());

    System.out.println("\n Périmètre du cercle : résultat attendu = 4");
    System.out.println(rectangle.perimetre());

    System.out.println("\n Surface du cercle : résultat attendu = 1");
    System.out.println(rectangle.surface());
  }
}
