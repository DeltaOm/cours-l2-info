package solverstests;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.function.BiFunction;
import solvers.TopologicalSorter;
import testutil.Util;
import java.util.HashMap;
import constraints.PrecedenceConstraint;
import constraints.Activity;
import java.util.HashSet;

public class TopologicalSorterTests
{
    public static int MAX_NB_ACTIVITIES;
    public static int MIN_DURATION;
    private static final HashSet<Activity> NO_ACTIVITY;
    private static final HashSet<PrecedenceConstraint> NO_CONSTRAINT;
    private final HashMap<Integer, Activity> activities;

    public TopologicalSorterTests() {
        this.activities = new HashMap<Integer, Activity>();
    }

    public boolean testBruteForceSort() {
        Util.log("TopologicalSorterTests", "testBruteForceSort");
        return Util.result(this.test(new TopologicalSorter()::bruteForceSort));
    }

    public boolean testLinearTimeSort() {
        Util.log("TopologicalSorterTests", "testLinearTimeSort");
        return Util.result(this.test(new TopologicalSorter()::linearTimeSort));
    }

    public boolean testSchedule() {
        Util.log("TopologicalSorterTests", "testSchedule");
        final TopologicalSorter topologicalSorter = new TopologicalSorter();
        boolean b = true && this.testSchedule(topologicalSorter, TopologicalSorterTests.NO_ACTIVITY, TopologicalSorterTests.NO_CONSTRAINT, true);
        for (int i = 1; i <= TopologicalSorterTests.MAX_NB_ACTIVITIES; ++i) {
            final HashSet<Activity> activities = this.getActivities(i);
            final HashSet<PrecedenceConstraint> linearConstraints = this.getLinearConstraints(i, false);
            final HashSet<PrecedenceConstraint> linearConstraints2 = this.getLinearConstraints(i, true);
            final HashSet<PrecedenceConstraint> fullDAGConstraints = this.getFullDAGConstraints(i, false);
            final HashSet<PrecedenceConstraint> fullDAGConstraints2 = this.getFullDAGConstraints(i, true);
            final HashSet<PrecedenceConstraint> cyclicConstraints = this.getCyclicConstraints(i, false);
            final HashSet<PrecedenceConstraint> cyclicConstraints2 = this.getCyclicConstraints(i, false);
            b = (b && this.testSchedule(topologicalSorter, activities, TopologicalSorterTests.NO_CONSTRAINT, true) && this.testSchedule(topologicalSorter, activities, linearConstraints, true) && this.testSchedule(topologicalSorter, activities, linearConstraints2, true) && this.testSchedule(topologicalSorter, activities, fullDAGConstraints, true) && this.testSchedule(topologicalSorter, activities, fullDAGConstraints2, true) && this.testSchedule(topologicalSorter, activities, cyclicConstraints, false) && this.testSchedule(topologicalSorter, activities, cyclicConstraints2, false));
        }
        return Util.result(b);
    }

    private boolean test(final BiFunction<HashSet<Activity>, HashSet<PrecedenceConstraint>, ArrayList<Activity>> biFunction) {
        boolean b = true && this.testOrdering(biFunction, TopologicalSorterTests.NO_ACTIVITY, TopologicalSorterTests.NO_CONSTRAINT, true);
        for (int i = 1; i <= TopologicalSorterTests.MAX_NB_ACTIVITIES; ++i) {
            final HashSet<Activity> activities = this.getActivities(i);
            final HashSet<PrecedenceConstraint> linearConstraints = this.getLinearConstraints(i, false);
            final HashSet<PrecedenceConstraint> linearConstraints2 = this.getLinearConstraints(i, true);
            final HashSet<PrecedenceConstraint> fullDAGConstraints = this.getFullDAGConstraints(i, false);
            final HashSet<PrecedenceConstraint> fullDAGConstraints2 = this.getFullDAGConstraints(i, true);
            final HashSet<PrecedenceConstraint> cyclicConstraints = this.getCyclicConstraints(i, false);
            final HashSet<PrecedenceConstraint> cyclicConstraints2 = this.getCyclicConstraints(i, false);
            b = (b && this.testOrdering(biFunction, activities, TopologicalSorterTests.NO_CONSTRAINT, true) && this.testOrdering(biFunction, activities, linearConstraints, true) && this.testOrdering(biFunction, activities, linearConstraints2, true) && this.testOrdering(biFunction, activities, fullDAGConstraints, true) && this.testOrdering(biFunction, activities, fullDAGConstraints2, true) && this.testOrdering(biFunction, activities, cyclicConstraints, false) && this.testOrdering(biFunction, activities, cyclicConstraints2, false));
        }
        return b;
    }

    private HashSet<Activity> getActivities(final int n) {
        final HashSet<Activity> set = new HashSet<Activity>();
        for (int i = 0; i < n; ++i) {
            set.add(this.getActivity(i));
        }
        return set;
    }

    private HashSet<PrecedenceConstraint> getLinearConstraints(final int n, final boolean b) {
        final HashSet<PrecedenceConstraint> set = new HashSet<PrecedenceConstraint>();
        for (int i = 0; i < n - 1; ++i) {
            set.add(new PrecedenceConstraint(this.getActivity(b ? (i + 1) : i), this.getActivity(b ? i : (i + 1))));
        }
        return set;
    }

    private HashSet<PrecedenceConstraint> getFullDAGConstraints(final int n, final boolean b) {
        final HashSet<PrecedenceConstraint> set = new HashSet<PrecedenceConstraint>();
        for (int i = 0; i < n; ++i) {
            for (int j = i + 1; j < n; ++j) {
                set.add(new PrecedenceConstraint(this.getActivity(b ? j : i), this.getActivity(b ? i : j)));
            }
        }
        return set;
    }

    private HashSet<PrecedenceConstraint> getCyclicConstraints(final int n, final boolean b) {
        final HashSet<PrecedenceConstraint> linearConstraints = this.getLinearConstraints(n, b);
        linearConstraints.add(new PrecedenceConstraint(this.getActivity(b ? 0 : (n - 1)), this.getActivity(b ? n : 0)));
        return linearConstraints;
    }

    private Activity getActivity(final int n) {
        if (!this.activities.containsKey(n)) {
            this.activities.put(n, new Activity("activity #" + n, TopologicalSorterTests.MIN_DURATION + n));
        }
        return this.activities.get(n);
    }

    private boolean testOrdering(final BiFunction<HashSet<Activity>, HashSet<PrecedenceConstraint>, ArrayList<Activity>> biFunction, final HashSet<Activity> obj, final HashSet<PrecedenceConstraint> obj2, final boolean b) {
        final ArrayList<Activity> obj3 = biFunction.apply(obj, obj2);
        if (b) {
            if (obj3 == null) {
                System.out.println("Test failed");
                System.out.println("Activities: " + obj);
                System.out.println("Constraints: " + obj2);
                System.out.println("Found: " + obj3);
                return false;
            }
            for (final PrecedenceConstraint precedenceConstraint : obj2) {
                if (obj3.indexOf(precedenceConstraint.getFirst()) > obj3.indexOf(precedenceConstraint.getSecond())) {
                    System.out.println("Test failed");
                    System.out.println("Activities: " + obj);
                    System.out.println("Constraints: " + obj2);
                    System.out.println("Found: " + obj3);
                    return false;
                }
            }
            return true;
        }
        else {
            if (obj3 != null) {
                System.out.println("Test failed");
                System.out.println("Activities: " + obj);
                System.out.println("Constraints: " + obj2);
                System.out.println("There is no solution but found: " + obj3);
                return false;
            }
            return true;
        }
    }

    private boolean testSchedule(final TopologicalSorter topologicalSorter, final HashSet<Activity> obj, final HashSet<PrecedenceConstraint> obj2, final boolean b) {
        final HashMap schedule = topologicalSorter.schedule((HashSet)obj, (HashSet)obj2);
        if (b) {
            if (schedule == null) {
                System.out.println("Test failed");
                System.out.println("Activities: " + obj);
                System.out.println("Constraints: " + obj2);
                System.out.println("Found: " + schedule);
                return false;
            }
            for (final PrecedenceConstraint precedenceConstraint : obj2) {
                if (schedule.get(precedenceConstraint.getSecond()) < schedule.get(precedenceConstraint.getFirst()) + precedenceConstraint.getFirst().getDuration()) {
                    System.out.println("Test failed");
                    System.out.println("Activities: " + obj);
                    System.out.println("Constraints: " + obj2);
                    System.out.println("Found: " + schedule);
                    return false;
                }
            }
            return true;
        }
        else {
            if (schedule != null) {
                System.out.println("Test failed");
                System.out.println("Activities: " + obj);
                System.out.println("Constraints: " + obj2);
                System.out.println("There is no solution but found: " + schedule);
                return false;
            }
            return true;
        }
    }

    static {
        TopologicalSorterTests.MAX_NB_ACTIVITIES = 10;
        TopologicalSorterTests.MIN_DURATION = 10;
        NO_ACTIVITY = new HashSet<Activity>();
        NO_CONSTRAINT = new HashSet<PrecedenceConstraint>();
    }
}
