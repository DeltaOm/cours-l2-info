package constraints;

import constraintstests.*;

public class Test{
  public static void main(String[] args) {
    boolean ok = true;
    ActivityTests activityTester = new ActivityTests();
    ok = ok && activityTester.testGetDescription();
    ok = ok && activityTester.testGetDuration();
    System.out.println(ok ? "All tests passed" : "At least one test failed");

    PrecedenceConstraintTests precedenceConstraintTester = new PrecedenceConstraintTests();
    ok = ok && precedenceConstraintTester.testGetFirst();
    ok = ok && precedenceConstraintTester.testGetSecond();
    ok = ok && precedenceConstraintTester.testIsSatisfiedBy();

    MeetConstraintTests meetConstraintTester = new MeetConstraintTests();
    ok = ok && meetConstraintTester.testGetFirst();
    ok = ok && meetConstraintTester.testGetSecond();
    ok = ok && meetConstraintTester.testIsSatisfiedBy();
  }
}
