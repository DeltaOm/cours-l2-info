package constraints;

public class PrecedenceConstraint{

  private Activity first;
  private Activity second;

  public PrecedenceConstraint(Activity first, Activity second){
    this.first = first;
    this.second = second;
  }

  public Activity getFirst(){
    return this.first;
  }

  public Activity getSecond(){
    return this.second;
  }

  public boolean isSatisfied(int date1, int date2){
    boolean ret = true;
    if(date1+this.first.getDuration() > date2){
      ret = false;
    }
    return ret;
  }

  public String toString(){
    return this.first+" "+this.second;
  }
}
