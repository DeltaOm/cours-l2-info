package constraints;

public class Activity{

  private String description;
  private int duration;

  public Activity(String description, int duration){
    this.description = description;
    this.duration = duration;
  }

  public String getDescription(){
    return this.description;
  }

  public int getDuration(){
    return this.duration;
  }

  public String toString(){
    return this.description+" "+this.duration;
  }
}
