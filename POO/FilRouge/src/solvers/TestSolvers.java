package solvers;

import solverstests.TopologicalSorterTests;

public class TestSolvers{
  public static void main(String[] args) {
    boolean ok = true;
    TopologicalSorterTests tester = new TopologicalSorterTests();
    ok = ok && tester.testBruteForceSort();
    ok = ok && tester.testLinearTimeSort();
    ok = ok && tester.testSchedule();
    System.out.println(ok ? "All tests passed" : "At least one test failed");
  }
}
