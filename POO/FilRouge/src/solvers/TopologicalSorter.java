package solvers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.HashMap;
import constraints.*;
import java.util.Map;

public class TopologicalSorter{

  public TopologicalSorter(){

  }

  public ArrayList<Activity> bruteForceSort(HashSet<Activity> activities, HashSet<PrecedenceConstraint> precedenceConstraints){
    HashSet<Activity> clone = new HashSet<Activity>(activities); //usage du copy constructor de HashSet
    ArrayList<Activity> ret = new ArrayList<Activity>();
    while (!clone.isEmpty()){
      boolean continuer = false;
      for(Activity act : activities){ //permet d'éviter une erreur lors du parcours de l'HashSet qu'on obient si on parcours dans clone
        boolean ok = true;
        for(PrecedenceConstraint cons : precedenceConstraints){
          if(cons.getSecond() == act && !ret.contains(cons.getFirst())){
            ok = false;
          }
        }
        //Break ne veut pas fonctionner, donc on vérifie que act n'est pas déjà dans ret
        if (ok && !ret.contains(act)){
          ret.add(act);
          clone.remove(act);
          continuer = true;
          break;
        }
      }
      if (!continuer){
        return null;
      }
    }
    return ret;
  }

  public HashMap<Activity, Integer> schedule(HashSet<Activity> activities, HashSet<PrecedenceConstraint> precedenceConstraints){
    ArrayList<Activity> ordonnee = bruteForceSort(activities,precedenceConstraints);
    HashMap<Activity,Integer> ret = new HashMap<Activity,Integer>();
    if(ordonnee == null){
      return null;
    }
    else if (ordonnee.size() == 0) {
      return ret;
    }
    else{
      for(int i = 0; i<ordonnee.size(); i++){
        Activity act = ordonnee.get(i);
        if(i == 0){
          ret.put(act,0);
        }
        else{
          ret.put(act, (ret.get(ordonnee.get(i-1)) + ordonnee.get(i-1).getDuration()));
        }
      }
    }
    return ret;
  }

  public ArrayList<Activity> linearTimeSort(HashSet<Activity> activities, HashSet<PrecedenceConstraint> precedenceConstraints){
    HashMap<Activity,Integer> nbPredecesseurs = new HashMap<Activity,Integer>();
    HashMap<Activity,ArrayList<Activity>> successeurs = new HashMap<Activity,ArrayList<Activity>>();
    int nb;

    for(Activity act: activities){
      nbPredecesseurs.put(act,0);
      successeurs.put(act,new ArrayList<Activity>());
    }

    for(PrecedenceConstraint cons : precedenceConstraints){
      if(cons.getFirst() != cons.getSecond()){
        nb = nbPredecesseurs.get(cons.getSecond());
        nb++;
        nbPredecesseurs.remove(cons.getSecond());
        nbPredecesseurs.put(cons.getSecond(),nb);

        ArrayList<Activity> succ = successeurs.get(cons.getFirst());
        succ.add(cons.getSecond());
        successeurs.remove(cons.getFirst());
        successeurs.put(cons.getFirst(),succ);
      }
      else{
        //On sort carrément le cas où o == o' en mettant nb à -1, il ne sera pas traité
        //En tout cas en faisant ça, le test passe :shrug:
        nb = nbPredecesseurs.get(cons.getSecond());
        nbPredecesseurs.remove(cons.getSecond());
        nbPredecesseurs.put(cons.getSecond(),-1);

        ArrayList<Activity> succ = successeurs.get(cons.getFirst());
        succ.add(cons.getSecond());
        successeurs.remove(cons.getFirst());
        successeurs.put(cons.getFirst(),succ);
      }
    }

      ArrayList<Activity> L = new ArrayList<Activity>();
      ArrayList<Activity> res = new ArrayList<Activity>();
      for(Activity act: activities){
        if(nbPredecesseurs.get(act) == 0){
          L.add(act);
        }
      }
      while(!L.isEmpty()){
        Activity o = L.get(0);
        res.add(o);
        L.remove(0);
        for(Activity op: successeurs.get(o)){
          nb = nbPredecesseurs.get(op);
          nb--;
          nbPredecesseurs.remove(op);
          nbPredecesseurs.put(op,nb);
          if(nb == 0){
            L.add(op);
          }
        }
      }
      if(res.size() == activities.size()){
        return res;
      }
      return null;
  }

}
