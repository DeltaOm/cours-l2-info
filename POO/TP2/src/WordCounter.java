package wordcounting;

import java.util.HashSet;
import java.util.HashMap;
import java.util.ArrayList;

public class WordCounter{

  private boolean casse;

  public WordCounter(boolean casse){
    this.casse = casse;
  }

  public HashSet<String> words(ArrayList<String> phrase){
    HashSet<String> ret = new HashSet<String>();
    int taille = phrase.size();
    for(int i = 0; i<taille; i++){
      //HashSet.add() n'ajoute que si l'élément n'est pas présent
      if(!this.casse){
        //Si on ignore la casse, on met en lower case
        ret.add(phrase.get(i).toLowerCase());
      }
      else{
        ret.add(phrase.get(i));
      }
    }

    return ret;
  }

  public HashMap<String,Integer> count(ArrayList<String> phrase){
    HashMap<String,Integer> ret = new HashMap<String,Integer>();
    String word;

    int taille = phrase.size();
    for(int i = 0; i<taille; i++){
      //On met en Lower case si casse == false
      if(!this.casse){
        word = phrase.get(i).toLowerCase();
      }
      else{
        word = phrase.get(i);
      }

      if(!ret.containsKey(word)){
        //Si la clé n'existe pas, on l'ajoute et on lui donne la valeur 1
        ret.put(word,1);
      }
      else{
        //Si la clé existe, on récupère la valeur et on ajoute 1, puis pour modifier la clé/valeur, on supprime et on la réajoute
        int value = ret.get(word) + 1;
        ret.remove(word);
        ret.put(word,value);
      }
    }
    return ret;
  }
}
