package phonebook;

import java.util.ArrayList;

public class PhoneBook{

  private ArrayList<Contact> contacts;

  public PhoneBook(){
    this.contacts = new ArrayList<Contact>();
  }

  public void addContact(String name, String phoneNumber){
    this.contacts.add(new Contact(name,phoneNumber));
  }

  public ArrayList<String> getPhoneNumbers(String name){
    Contact contact;
    ArrayList<String> ret = new ArrayList<String>();
    int taille = contacts.size();
    for(int i = 0; i<taille; i++){
      contact = contacts.get(i);
      if(contact.getName().equals(name)){
        ret.add(contact.getPhoneNumber());
      }
    }
    return ret;
  }

  public ArrayList<String> getNames(String phoneNumber){
    Contact contact;
    ArrayList<String> ret = new ArrayList<String>();
    int taille = contacts.size();
    for(int i = 0; i<taille; i++){
      contact = contacts.get(i);
      if(contact.getPhoneNumber().equals(phoneNumber)){
        ret.add(contact.getName());
      }
    }
    return ret;
  }
}
