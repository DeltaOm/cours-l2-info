import java.util.ArrayList;
import java.util.Arrays;
import wordcounting.WordCounter;

public class Demo{
  public static void main(String[] args) {
    WordCounter wordCounterFalse = new WordCounter(false);
    WordCounter wordCounterTrue = new WordCounter(true);

    System.out.println("Si on ignore la casse");
    System.out.println(wordCounterFalse.words(sentence));
    System.out.println(wordCounterFalse.count(sentence));

    System.out.println("\n Si on n'ignore pas la casse");
    System.out.println(wordCounterTrue.words(sentence));
    System.out.println(wordCounterTrue.count(sentence));
  }
}
