import phonebook.*;
import java.util.ArrayList;

public class DemoOriginal{

  public static void main(String[] args) {
    Contact c1 = new Contact("Test","0298578474");
    System.out.println(c1.toString());
    System.out.println("Nom :"+c1.getName()+" numéro :"+c1.getPhoneNumber());

    PhoneBook phonebook = new PhoneBook();
    phonebook.addContact("Test2","0202020202");
    phonebook.addContact("Test3","0303030303");
    phonebook.addContact("Test2","0404040404");
    phonebook.addContact("Test4","0202020202");

    System.out.println(phonebook.toString());
    System.out.println(phonebook.getPhoneNumbers("Test2"));
    System.out.println(phonebook.getNames("0202020202"));
  }
}
